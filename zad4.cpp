/*
Qsxcftgb
Sprawozdanie Lab 4
Michal Dos
ocena 3.5 

1. Cel ćwiczenia

Celem ćwiczenia było napisanie programu, który za pomocą algorytmu Schrage będzie uszeregowywał zbiór zadań w taki sposób, by łączny czas wykonywania całego zbioru zadań był jak najkrótszy.


2. Algorytm Schrage

Algorytm Schrage polega na swoim podzieleniu zbioru zadań na 3 mniejsze podzbiory: zbiór zadań wykonanych, zbiór zadań dostępnych i na zbiór zadań niedostępnych. W każdej jednostce czasu przenosimy z zbioru zadań niedostępnych do zbioru zadań dostępnych te zadania,
których czas dostępności r jest mniejszy lub równy obecnej jednostce czasu.
Kolejnym krokiem jest wybranie z zbioru zadań dostępnych zadania o największym czasie dostarczenia q
i wykonanie go, po czym przenosimy te zadanie do zbioru zadań wykonanych.
Algorytm wykonywany jest, dopóki wszystkie zadania nie są przeniesione do zbioru zadań wykonanych.
Kolejność dokładania kolejnych zadań do zbioru zadań wykonanych jest kolejnością o najmniejszym czasie
Cmax dla danego zbioru zadań.
Czas Cmax jest liczony w następujący sposób: na samym początku działania algorytmu wartość Cmax jest
równa 0. Po wykonaniu każdego zadania wartości Cmax przypisujemy wartość większą z 2 wartości:
obecnego Cmax oraz T+q, gdzie T to obecna jednostka czasu a q to czas dostarczenia dopiero co wykonanego
zadania.



3. Algorytm Schrage z podziałem

Modyfikacją algorytmu Schrage jest algorytm Schrage z podziałem. W przypadku zwykłego algorytmu
Schrage, algorytm nie mógł przerwać wykonywania zadania, natomiast algorytm Schrage z podziałem ma
taką możliwość.
Sposób działania tego algorytmu jest bardzo podobny do działania algorytmu Schrage bez podziału. Jedyna
modyfikacja ma miejsce w przenoszeniu zadań z zbioru zadań dostępnych do zbioru zadań wykonanych.
W każdej jednostce czasu wyszukiwane jest zadanie o największym czasie dostarczenia q spośród zbioru
zadań dostępnych oraz obecnie wykonywanego zadania. Jeśli pojawiło się zadanie o większym czasie q,
to obecne zadanie wraca do zbioru zadań dostępnych a zaczyna się wykonywać zadanie o większym czasie q.
Przy używaniu algorytmu Schrage z podziałem nie ma możliwości podania kolejności wykonywania zadań
dla dowolnego zbioru zadań.
Wynik Cmax otrzymany za pomocą algorytmu Schrage z podziałem jest optymalny dla danego zbioru
zadań. Wyniki otrzymane za pomocą algorytmu Schrage nie mogą być lepsze od wyników otrzymanych za
pomocą algorytmu Schrage z podziałem.


Dzialanie programu

 data.000:
| Podzial: 221
| Schrage: 283
| Roznica: 62
1 3 4 2 
===========================================
| data.001:
| Podzial: 3026
| Schrage: 3109
| Roznica: 83
1 45 30 28 18 25 10 21 6 48 5 13 31 7 2 4 49 11 19 33 46 32 47 23 34 50 42 14 22 29 43 8 9 36 40 37 17 38 20 39 16 24 3 35 12 41 27 44 15 26 
===========================================
| data.002:
| Podzial: 3654
| Schrage: 3708
| Roznica: 54
38 1 28 6 22 10 20 35 37 11 21 49 3 29 46 17 43 18 19 13 25 5 47 27 7 32 34 14 26 16 40 44 41 33 9 8 50 45 24 39 2 42 31 12 30 23 48 4 15 36 
===========================================
| data.003:
| Podzial: 3309
| Schrage: 3353
| Roznica: 44
2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 1 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 
===========================================
| data.004:
| Podzial: 3172
| Schrage: 3235
| Roznica: 63
1 34 39 15 32 41 26 5 46 37 33 17 24 27 21 20 42 43 48 13 29 36 4 6 16 50 10 25 18 2 38 19 9 40 44 28 11 49 3 7 30 8 47 31 35 12 45 23 22 14 
===========================================
| data.005:
| Podzial: 3618
| Schrage: 3625
| Roznica: 7
1 44 25 35 28 4 30 45 43 46 10 9 2 40 16 17 3 20 8 27 38 14 6 36 7 21 48 5 37 24 26 47 50 41 23 12 22 13 39 34 18 11 49 15 19 42 32 33 31 29 
===========================================
| data.006:
| Podzial: 3439
| Schrage: 3446
| Roznica: 7
19 36 46 4 38 39 5 6 26 3 29 8 40 15 11 9 12 13 49 2 44 28 42 47 10 30 50 23 43 27 35 17 14 37 18 16 45 41 31 32 21 7 24 33 22 25 20 1 34 48 
===========================================
| data.007:
| Podzial: 3820
| Schrage: 3862
| Roznica: 42
48 1 41 40 28 46 44 23 34 29 10 38 49 36 15 50 16 21 2 19 35 9 8 42 25 37 7 45 12 20 18 32 26 43 13 6 5 4 31 17 22 24 14 27 39 30 47 3 33 11 
===========================================
| data.008:
| Podzial: 3633
| Schrage: 3645
| Roznica: 12
30 49 5 29 37 14 22 8 11 16 4 25 15 19 23 18 35 39 2 28 3 26 47 27 24 42 20 12 10 41 45 34 32 33 50 46 43 9 44 1 13 48 7 36 38 6 21 40 17 31 
===========================================

*/

#include <iostream>
#include<fstream>
#include <string>


/**
 * @brief struktura zadania
 *  ID Numer identyfikacyjny zadania
    r: Czas pojawienia się zadania 
    p: Czas przetwarzania zadania
    q: Termin wykonania zadania
 * 
 */
struct task
{
  public:
    int ID;
    int r;
    int p;
    int q;
};

/**
 * @brief funkcja zamienia miejscami pola dwoch obiektow typu task
 * 
 * @param a zadanie a
 * @param b zadanie b
 */
void task_swap(task& a, task& b)
{
  std::swap(a.ID, b.ID);
  std::swap(a.p, b.p);
  std::swap(a.q, b.q);
  std::swap(a.r, b.r);
}

/**
 * @brief Implementuje algorytm Schrage
 * 
 * @param n liczba zadan
 * @param T tablica zadan 
 * @param X tablica kolejnosci
 * @return int cmax
 */
int schrage(int n, task* T, int* X)
{
  int ND[100], D[100];							//ND - tablica zadań niedostępnych  D - tablica zadań dostępnych
  int nd = n, d = 0, w = 0;		// nd - liczba zadań niedostępnych  d-tablica zadań dostępnych  w-liczba zadań wykonanych 
  int  t = 0, cmax = 0;			//t - obecna jednostka czasu  cmax-długość najdłuższego uszeregowania
  for (int i = 0; i < n; i++)
  {
    ND[i] = i;
  }
  for (int i = 0; i < n - 1; i++)			//sortowanie tablicy zadań niedostęnych malejąco po r
  {
    for (int j = 0; j < n - 1; j++)
    {
      if (T[ND[j]].r < T[ND[j + 1]].r)
      {
        std::swap(ND[j], ND[j + 1]);
      }
    }
  }
  while (w != n)					//jeśli są zadania niewykonane
  {
    if (nd != 0)				//jeśli są jeszcze zadania niedostępne
    {	
      if (T[ND[nd - 1]].r <= t)		//jeśli jest jakieś task które przyszło
      {
        D[d] = ND[nd - 1];
        d++;
        nd--;
        for (int k = d - 1; k > 0; k--)			//sortowanie tablicy zadań dostępnych rosnąco po q
        {
          if (T[D[k]].q < T[D[k - 1]].q)
          {
            std::swap(D[k], D[k - 1]);
          }
        }
        continue;
      }
    }
    if (d != 0)			//jeśli są zadania dostępne
    {
      X[w] = D[d - 1];
      t += T[X[w]].p;
      cmax = std::max(cmax, t + T[X[w]].q);
      d--;
      w++;
      continue;
    }
    if (d == 0 && T[ND[nd - 1]].r > t)			//jeśli jest jakiaś przerwa w pracy to ją przeskocz
    {
      t = T[ND[nd - 1]].r;
    }
  }
  return cmax;
}


/**
 * @brief 
 * 
 * @param n 
 * @param T 
 * @return int 
 */
int schrage_divide(int n, task* T)
{
  int ND[100];
  int D[100];
  int pom[100];
  int nd = n;
  int d = 0;
  int  t = 0;
  int cmax = 0;
  int poz = 100;
  int ile_zr = 0;
  for (int i = 0; i < n; i++)
  {
    pom[i] = T[i].p;
  }
  for (int i = 0; i < n; i++)
  {
    ND[i] = i;
  }
  for (int i = 0; i < n - 1; i++)			
  {
    for (int j = 0; j < n - 1; j++)
    {
      if (T[ND[j]].r < T[ND[j + 1]].r)
      {
        std::swap(ND[j], ND[j + 1]);
      }
    }
  }
  while (nd != 0 || d != 0)  
  {
    if (nd != 0) // dopoki sa dostepne zadania
    {
      if (T[ND[nd-1]].r <= t) // sprawdza czy mozna zaplanowac zadanie
      {
        D[d] = ND[nd - 1];
        d++;
        nd--;
        for (int k = d - 1; k > 0; k--)			
        {
          if (T[D[k]].q < T[D[k - 1]].q)
          {
            std::swap(D[k], D[k - 1]);
          }
        }
        if (poz != 100)
        {
          if (T[D[d - 1]].q > T[poz].q)
          {
            D[d] = poz;
            std::swap(D[d], D[d - 1]);
            d++;						
            poz = 100;
          }
        }
        continue;
      }
    }
    if (d != 0)
    {
      if (poz == 100)
      {
        poz = D[d - 1];
        d--;
      }
      if (nd != 0)
      {
        ile_zr = std::min(pom[poz], T[ND[nd - 1]].r - t);
      }
      else
      {
        ile_zr = pom[poz];
      }
      t += ile_zr;
      pom[poz] -= ile_zr;
      if (pom[poz]== 0)
      {
        cmax = std::max(cmax, t + T[poz].q);
        poz = 100;
      }
      continue;
    }
    if (d == 0 && nd != 0)
    {
      if (T[ND[nd - 1]].r > t)
      {
        t = T[ND[nd - 1]].r;
      }
    }
  }
  return cmax;
}
int main()
{
  int X[100];
  task T[100];
  std::string s = "data.00", s1, s2;
  std::ifstream f("data.txt");
  int n;
  for (int i = 0; i < 9; i++)			//pętla przechodząca przez każdy zbiór danych
  {
    s1 = s + std::to_string(i) + ":";
    while (s2 != s1)				//szukanie odpowiedniego zbioru danych
    {
      f >> s2;
    }
    f >> n;							//wczytywanie ilości zadań w danym zbiorze danych
    for (int j = 0; j < n; j++)		//wczytywanie parametrów kolejnych zadań
    {
      T[j].ID = j;
      f >> T[j].r >> T[j].p >> T[j].q;
    }
    std::cout << "| " << s1 << std::endl;
    std::cout << "| Podzial: " << schrage_divide(n, T) << std::endl;
    std::cout << "| Schrage: " << schrage(n, T, X) << std::endl;
    std::cout << "| Roznica: " << schrage(n, T, X) - schrage_divide(n, T) << std::endl;
    for (int i = 0; i < n; i++)
    {
      std::cout << X[i]+1 << " ";
    }
    std::cout <<std::endl<< "===========================================" <<std::endl;
  }
}